﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

#region DockPanel
using WeifenLuo.WinFormsUI.Docking;
#endregion

namespace PawnPlus
{
    public partial class Output : DockContent
    {
        public Output()
        {
            InitializeComponent();
        }

        private void OutBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu contextMenu = new System.Windows.Forms.ContextMenu();
                MenuItem menuItem = new MenuItem("Copy");
                menuItem.Click += new EventHandler(CopyAction);
                contextMenu.MenuItems.Add(menuItem);

                OutBox.ContextMenu = contextMenu;
            }
        }

        private void OutBox_DoubleClick(object sender, EventArgs e)
        {
            if (OutBox.Text.Length > 0)
            {
                SendKeys.Send("{HOME}+{END}");
                SendKeys.Flush();

                string selected = OutBox.SelectedText;

                foreach (Match match in Regex.Matches(selected, @"\bline\s(.+)\."))
                {
                    GlobalVars.GTLine = int.Parse(match.Groups[1].ToString());
                    GlobalVars.GTLActive = true;
                }
            }
        }

        private void CopyAction(object sender, EventArgs e)
        {
            OutBox.Copy();
        }
    }
}
