﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

#region DockPanel
using WeifenLuo.WinFormsUI.Docking;
#endregion

#region DigitalRune
using DigitalRune.Windows.TextEditor;
using DigitalRune.Windows.TextEditor.Actions;
using DigitalRune.Windows.TextEditor.Completion;
using DigitalRune.Windows.TextEditor.Document;
using DigitalRune.Windows.TextEditor.Formatting;
using DigitalRune.Windows.TextEditor.Highlighting;
using DigitalRune.Windows.TextEditor.Insight;
using DigitalRune.Windows.TextEditor.Markers;
using DigitalRune.Windows.TextEditor.Selection;
using DigitalRune.Windows.TextEditor.Folding;
#endregion

namespace PawnPlus
{
    public partial class CodeEditor : DockContent
    {
        ToolTip InfoMessage = new ToolTip();

        public CodeEditor()
        {
            InitializeComponent();

            string SyntaxFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Syntax");
            HighlightingManager.Manager.AddSyntaxModeFileProvider(new FileSyntaxModeProvider(SyntaxFolder));

            CloseButton = false;
            CloseButtonVisible = false;

        }

        private void CodeEditor_Load(object sender, EventArgs e)
        {
            CodeBox.Document.HighlightingStrategy = HighlightingManager.Manager.FindHighlighter("PAWN");
            CodeBox.Document.FormattingStrategy = new CSharpFormattingStrategy();

            CodeBox.Document.FoldingManager.FoldingStrategy = new CodeFoldingStrategy();
        }

        private void CodeBox_DocumentChanged(object sender, DocumentEventArgs e)
        {
            GlobalVars.FileSaved = false;
        }

        public void FindPrevious(string Text, bool IsFind, bool CaseSensitive, bool MatchWord)
        {
            if (string.IsNullOrEmpty(Text))
                return;

            CodeBox.Select();

            IDocument CodeBoxFind = CodeBox.Document;

            if (CaseSensitive == true)
            {
                int offset = CodeBoxFind.TextContent.LastIndexOf(Text.Trim(), CodeBox.ActiveTextAreaControl.TextArea.Caret.Offset);
                CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                if (offset >= 0)
                {
                    TextLocation startLocation = CodeBoxFind.OffsetToPosition(offset);
                    TextLocation endLocation = CodeBoxFind.OffsetToPosition(offset + Text.Trim().Length);
                    CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);

                    string RequestText = CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SelectedText;
                    CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                    if (String.Compare(RequestText.Trim(), GlobalVars.FindText.Trim()) == 0 && IsFind == true)
                    {
                        CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);
                        CodeBox.ActiveTextAreaControl.Caret.Position = endLocation;
                    }
                    else if (String.Compare(RequestText.Trim(), GlobalVars.ReplaceWith.Trim()) == 0 && IsFind == false)
                    {
                        CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);
                        CodeBox.ActiveTextAreaControl.Caret.Position = endLocation;
                    }
                }
            }
            else if (MatchWord == true)
            {
                bool OnlyWords = false;

                Regex Check = new Regex("^[a-zA-Z ]+$");

                if (IsFind == true)
                {
                    if (Check.IsMatch(GlobalVars.FindText))
                        OnlyWords = true;
                    else
                        OnlyWords = false;

                    if (OnlyWords == true)
                    {
                        int offset = CodeBoxFind.TextContent.LastIndexOf(Text.Trim(), CodeBox.ActiveTextAreaControl.TextArea.Caret.Offset);
                        CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                        if (offset >= 0)
                        {
                            TextLocation startLocation = CodeBoxFind.OffsetToPosition(offset);
                            TextLocation endLocation = CodeBoxFind.OffsetToPosition(offset + Text.Trim().Length);
                            CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);

                            string RequestText = CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SelectedText;

                            if (String.Compare(RequestText.Trim(), GlobalVars.FindText.Trim()) == 0)
                                CodeBox.ActiveTextAreaControl.Caret.Position = endLocation;
                        }
                    }
                }
                else
                {
                    if (Check.IsMatch(GlobalVars.ReplaceText))
                        OnlyWords = true;
                    else
                        OnlyWords = false;

                    if (OnlyWords == true)
                    {
                        int offset = CodeBoxFind.TextContent.LastIndexOf(Text.Trim(), CodeBox.ActiveTextAreaControl.TextArea.Caret.Offset);
                        CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                        if (offset >= 0)
                        {
                            TextLocation startLocation = CodeBoxFind.OffsetToPosition(offset);
                            TextLocation endLocation = CodeBoxFind.OffsetToPosition(offset + Text.Trim().Length);
                            CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);

                            string RequestText = CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SelectedText;

                            if (String.Compare(RequestText.Trim(), GlobalVars.ReplaceText.Trim()) == 0)
                                CodeBox.ActiveTextAreaControl.Caret.Position = endLocation;
                        }
                    }
                }
            }
            else
            {
                int offset = CodeBoxFind.TextContent.ToLower().LastIndexOf(Text.ToLower().Trim(), CodeBox.ActiveTextAreaControl.TextArea.Caret.Offset);
                CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                if (offset >= 0)
                {
                    TextLocation startLocation = CodeBoxFind.OffsetToPosition(offset);
                    TextLocation endLocation = CodeBoxFind.OffsetToPosition(offset + Text.Trim().Length);
                    CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);
                    CodeBox.ActiveTextAreaControl.Caret.Position = endLocation;
                }
            }
        }

        private void CodeBox_Enter(object sender, EventArgs e)
        {
            GlobalVars.IsCodeBoxActive = true;
        }

        private void FindBox_Enter(object sender, EventArgs e)
        {
            GlobalVars.IsCodeBoxActive = false;
        }

        public void Replace(string Text, string ReplaceText, bool replaceAll, bool ReplacePrevious, bool CaseSensitive, bool MatchWord)
        {
            if (ReplacePrevious == false)
            {
                if (string.IsNullOrEmpty(Text))
                    return;

                CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                int offset;

                if (!replaceAll)
                {
                    if (!CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.HasSomethingSelected)
                        FindNextMatch(Text, false, CaseSensitive, MatchWord);

                    try
                    {
                        offset = CodeBox.Document.PositionToOffset(CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.Selections[0].StartPosition);

                        if (offset > 0)
                            CodeBox.Document.Replace(offset, Text.Trim().Length, ReplaceText.Trim());
                    }
                    catch { }
                }
                else
                {
                    FindNextMatch(Text, false, CaseSensitive, MatchWord);

                    while (CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.HasSomethingSelected)
                    {
                        offset = CodeBox.Document.PositionToOffset(CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.Selections[0].StartPosition);

                        if (offset > 0)
                            CodeBox.Document.Replace(offset, Text.Trim().Length, ReplaceText.Trim());

                        FindNextMatch(Text, false, CaseSensitive, MatchWord);
                    }
                }
                CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();
            }

            if (ReplacePrevious == true)
            {
                if (string.IsNullOrEmpty(Text))
                    return;

                CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                int offset;

                if (!CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.HasSomethingSelected)
                    FindPrevious(Text, false, CaseSensitive, MatchWord);

                try
                {
                    offset = CodeBox.Document.PositionToOffset(CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.Selections[0].StartPosition);

                    if (offset > 0)
                        CodeBox.Document.Replace(offset, Text.Trim().Length, ReplaceText.Trim());
                }
                catch { }
                CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();
            }
        } 
        public void FindNextMatch(string Text, bool IsFind, bool CaseSensitive, bool MatchWord)
        {
            if (string.IsNullOrEmpty(Text))
                return;

            CodeBox.Select();

            IDocument CodeBoxFind = CodeBox.Document;

            if(CaseSensitive == true)
            {
                int offset = CodeBoxFind.TextContent.IndexOf(Text.Trim(), CodeBox.ActiveTextAreaControl.TextArea.Caret.Offset);
                CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                if (offset >= 0)
                {
                    TextLocation startLocation = CodeBoxFind.OffsetToPosition(offset);
                    TextLocation endLocation = CodeBoxFind.OffsetToPosition(offset + Text.Trim().Length);
                    CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);

                    string RequestText = CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SelectedText;
                    CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                    if (String.Compare(RequestText.Trim(), GlobalVars.FindText.Trim()) == 0 && IsFind == true)
                    {
                        CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);
                        CodeBox.ActiveTextAreaControl.Caret.Position = endLocation;
                    }
                    else if(String.Compare(RequestText.Trim(), GlobalVars.ReplaceWith.Trim()) == 0 && IsFind == false)
                    {
                        CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);
                        CodeBox.ActiveTextAreaControl.Caret.Position = endLocation;
                    }
                }
            }
            else if(MatchWord == true)
            {
                bool OnlyWords = false;

                Regex Check = new Regex("^[a-zA-Z ]+$");

                if (IsFind == true)
                {
                    if (Check.IsMatch(GlobalVars.FindText))
                        OnlyWords = true;
                    else
                        OnlyWords = false;

                    if (OnlyWords == true)
                    {
                        int offset = CodeBoxFind.TextContent.IndexOf(Text.Trim(), CodeBox.ActiveTextAreaControl.TextArea.Caret.Offset);
                        CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                        if (offset >= 0)
                        {
                            TextLocation startLocation = CodeBoxFind.OffsetToPosition(offset);
                            TextLocation endLocation = CodeBoxFind.OffsetToPosition(offset + Text.Trim().Length);
                            CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);

                            string RequestText = CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SelectedText;

                            if (String.Compare(RequestText.Trim(), GlobalVars.FindText.Trim()) == 0)
                                CodeBox.ActiveTextAreaControl.Caret.Position = endLocation;
                        }
                    }
                }
                else
                {
                    if (Check.IsMatch(GlobalVars.ReplaceText))
                        OnlyWords = true;
                    else
                        OnlyWords = false;

                    if (OnlyWords == true)
                    {
                        int offset = CodeBoxFind.TextContent.IndexOf(Text.Trim(), CodeBox.ActiveTextAreaControl.TextArea.Caret.Offset);
                        CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                        if (offset >= 0)
                        {
                            TextLocation startLocation = CodeBoxFind.OffsetToPosition(offset);
                            TextLocation endLocation = CodeBoxFind.OffsetToPosition(offset + Text.Trim().Length);
                            CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);

                            string RequestText = CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SelectedText;

                            if (String.Compare(RequestText.Trim(), GlobalVars.ReplaceText.Trim()) == 0)
                                CodeBox.ActiveTextAreaControl.Caret.Position = endLocation;
                        }
                    }
                }
            }
            else
            {
                int offset = CodeBoxFind.TextContent.ToLower().IndexOf(Text.ToLower().Trim(), CodeBox.ActiveTextAreaControl.TextArea.Caret.Offset);
                CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.ClearSelection();

                if (offset >= 0)
                {
                    TextLocation startLocation = CodeBoxFind.OffsetToPosition(offset);
                    TextLocation endLocation = CodeBoxFind.OffsetToPosition(offset + Text.Trim().Length);
                    CodeBox.ActiveTextAreaControl.TextArea.SelectionManager.SetSelection(startLocation, endLocation);
                    CodeBox.ActiveTextAreaControl.Caret.Position = endLocation;
                }
            }
        }
    }
}
