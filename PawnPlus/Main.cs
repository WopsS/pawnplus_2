﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

#region DockPanel
using WeifenLuo.WinFormsUI.Docking;
#endregion

#region DigitalRune
using DigitalRune.Windows.TextEditor;
using DigitalRune.Windows.TextEditor.Actions;
using DigitalRune.Windows.TextEditor.Completion;
using DigitalRune.Windows.TextEditor.Document;
using DigitalRune.Windows.TextEditor.Formatting;
using DigitalRune.Windows.TextEditor.Highlighting;
using DigitalRune.Windows.TextEditor.Insight;
using DigitalRune.Windows.TextEditor.Markers;
using DigitalRune.Windows.TextEditor.Selection;
using DigitalRune.Windows.TextEditor.Folding;
#endregion

namespace PawnPlus
{
    public partial class Main : Form
    {
        #region Forms
        CodeEditor codeeditor = new CodeEditor();
        Output output = new Output();

        FindReplace findreplace = new FindReplace();
        #endregion

        DeserializeDockContent DockContentLayout;

        public Main()
        {
            InitializeComponent();

            DockContentLayout = new DeserializeDockContent(GetLayout);

            Languages.Language("English");

            this.MaximumSize = Screen.PrimaryScreen.WorkingArea.Size;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            
            /*Microsoft.Win32.RegistryKey key;
            key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("Software\\PawnPlus");
            key.SetValue("Name", "WopsS");
            key.Close();

            RegistryKey rk = Registry.CurrentUser;
            RegistryKey sk1 = rk.OpenSubKey("Software\\PawnPlus");

            MessageBox.Show(sk1.GetValue("Name").ToString());*/

            var s1 = Stopwatch.StartNew();

            #region LoadLayout
            string LayoutFile = Path.Combine(GlobalVars.ApplicationDataPath, "Layout.xml");

            if (File.Exists(LayoutFile))
            {
                dockPanel.LoadFromXml(LayoutFile, DockContentLayout);
            }
            else
            {
                codeeditor.Show(dockPanel, DockState.Document);
                output.Show(dockPanel, DockState.DockBottom);
            }
            #endregion

            s1.Stop();
            //MessageBox.Show(s1.Elapsed.TotalMilliseconds.ToString());

            BetaLabel.Text = Languages.Version + " " + +GlobalVars.CurrentVersion.Major + "." + GlobalVars.CurrentVersion.Minor + "." + GlobalVars.CurrentVersion.Build + " " + Languages.Beta;
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            #region Savelayout
            string configFile = Path.Combine(GlobalVars.ApplicationDataPath, "Layout.xml");

            if (File.Exists(configFile))
                File.Delete(configFile);

            dockPanel.SaveAsXml(configFile);
            #endregion

            if (GlobalVars.FileSaved == false)
            {
                DialogResult AskForSave = new DialogResult();
                AskForSave = MessageBox.Show(Languages.SaveChanges, Languages.SaveChangesTitle, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                StatutsBar.BackColor = GlobalVars.IdleColor;
                StatutsLabel.Text = Languages.SavingFile;

                if (AskForSave == DialogResult.Yes)
                {
                    if (Timer.Enabled)
                    {
                        Timer.Stop();
                    }
                    StatutsLabel.Text = Languages.SavingFile;
                    saveFileDialog.ShowDialog();
                }

                if (AskForSave == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    StatutsBar.BackColor = GlobalVars.ReadyColor;
                    StatutsLabel.Text = Languages.Ready;
                }
            }
        }

        private IDockContent GetLayout(string LayoutString)
        {
            if (LayoutString == typeof(Output).ToString())
                return output;
            else
                return codeeditor;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            #region CodeEditor
            // Update Floding.
            codeeditor.CodeBox.Document.FoldingManager.UpdateFolds(null, null);

            if(GlobalVars.GTLActive == true)
            {
                GlobalVars.GTLActive = false;
                codeeditor.CodeBox.ActiveTextAreaControl.Caret.Position = new TextLocation(10000, GlobalVars.GTLine - 1); // 10,000 for last column of row.
                codeeditor.CodeBox.Focus();
            }

            if (GlobalVars.FindClicked == true)
            {
                GlobalVars.FindClicked = false;

                codeeditor.FindNextMatch(GlobalVars.FindText, true, findreplace.casesensitive.Checked, findreplace.matchwholeword.Checked);
            }

            if (GlobalVars.ReplaceClicked == true)
            {
                GlobalVars.ReplaceClicked = false;

                codeeditor.Replace(GlobalVars.ReplaceText, GlobalVars.ReplaceWith, false, false, findreplace.casesensitive2.Checked, findreplace.matchwholeword2.Checked);
            }

            if (GlobalVars.ReplacePreviousClicked == true)
            {
                GlobalVars.ReplacePreviousClicked = false;

                codeeditor.Replace(GlobalVars.ReplaceText, GlobalVars.ReplaceWith, false, true, findreplace.casesensitive2.Checked, findreplace.matchwholeword2.Checked);
            }

            if (GlobalVars.ReplaceAllClicked == true)
            {
                GlobalVars.ReplaceAllClicked = false;

                codeeditor.Replace(GlobalVars.ReplaceText, GlobalVars.ReplaceWith, true, false, findreplace.casesensitive2.Checked, findreplace.matchwholeword2.Checked);
            }
            GlobalVars.MaximLines = codeeditor.CodeBox.Document.TotalNumberOfLines;
            #endregion

            LineLabel.Text = "Line " + (codeeditor.CodeBox.ActiveTextAreaControl.TextArea.Caret.Line + 1).ToString();
            ColumnLabel.Text = "Column " + (codeeditor.CodeBox.ActiveTextAreaControl.TextArea.Caret.Column + 1).ToString();
        }

        private void gamemodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            codeeditor.CodeBox.Document.TextContent = File.ReadAllText(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath) + "\\SAMP", "Gamemode.pwn"));
        }

        private void filterscriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            codeeditor.CodeBox.Document.TextContent = File.ReadAllText(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath) + "\\SAMP", "Filterscript.pwn"));
        }

        private void blankToolStripMenuItem_Click(object sender, EventArgs e)
        {
            codeeditor.CodeBox.Document.TextContent = File.ReadAllText(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath) + "\\SAMP", "Blank.pwn"));
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            #region AskForSave

            if (GlobalVars.FileSaved == false)
            {
                DialogResult AskForSave = new DialogResult();

                AskForSave = MessageBox.Show(Languages.SaveChanges, Languages.SaveChangesTitle, MessageBoxButtons.YesNoCancel);

                if (AskForSave == DialogResult.Yes)
                    saveFileDialog.ShowDialog();

                if (AskForSave != DialogResult.Cancel) { }
            }
            #endregion

            StatutsBar.BackColor = GlobalVars.IdleColor;
            StatutsLabel.Text = Languages.OpeningFile;

            DialogResult OpenDialog = new DialogResult();
            OpenDialog = openFileDialog.ShowDialog();

            if (OpenDialog == DialogResult.OK)
            {
                GlobalVars.OpenedFilePath = openFileDialog.FileName;

                codeeditor.CodeBox.Document.TextContent = File.ReadAllText(GlobalVars.OpenedFilePath, Encoding.Default);

                StatutsBar.BackColor = GlobalVars.InfoColor;
                StatutsLabel.Text = Languages.FileOpened;

                GlobalVars.FileSaved = true;
            }
            else
            {
                StatutsBar.BackColor = GlobalVars.InfoColor;
                StatutsLabel.Text = Languages.OpeningCanceled;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (GlobalVars.CompilerWorking == false)
            {
                if (codeeditor.CodeBox.Document.TextLength > 0)
                {
                    if (GlobalVars.OpenedFilePath != null)
                    {
                        StatutsLabel.Text = Languages.SavingFile;

                        File.WriteAllText(GlobalVars.OpenedFilePath, codeeditor.CodeBox.Text, Encoding.Default);

                        StatutsLabel.Text = Languages.FileSaved;
                        GlobalVars.FileSaved = true;
                    }
                    else
                    {
                        StatutsLabel.Text = Languages.SavingFile;

                        DialogResult SaveDialog = new DialogResult();
                        SaveDialog = saveFileDialog.ShowDialog();

                        if (SaveDialog == DialogResult.OK)
                        {
                            StatutsBar.BackColor = GlobalVars.InfoColor;
                            StatutsLabel.Text = Languages.FileSaved;
                        }
                        else
                        {
                            StatutsBar.BackColor = GlobalVars.InfoColor;
                            StatutsLabel.Text = Languages.SavingCanceled;
                        }
                    }
                }
                else
                {
                    StatutsBar.BackColor = GlobalVars.ErrorColor;
                    StatutsLabel.Text = Languages.SaveEmptyFile;
                }
            }
            else
            {
                SystemSounds.Beep.Play();
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StatutsBar.BackColor = GlobalVars.IdleColor;
            StatutsLabel.Text = Languages.SavingFile;

            DialogResult SaveDialog = new DialogResult();
            SaveDialog = saveFileDialog.ShowDialog();

            if (SaveDialog == DialogResult.OK)
            {
                StatutsBar.BackColor = GlobalVars.InfoColor;
                StatutsLabel.Text = Languages.FileSaved;
                GlobalVars.FileSaved = true;
            }
            else
            {
                StatutsBar.BackColor = GlobalVars.InfoColor;
                StatutsLabel.Text = Languages.SavingCanceled;
            }
        }

        private void closeFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(Path.Combine(GlobalVars.ApplicationDataPath, "LastFileOpened.txt")))
            {
                File.Delete(Path.Combine(GlobalVars.ApplicationDataPath, "LastFileOpened.txt"));
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            codeeditor.CodeBox.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            codeeditor.CodeBox.Redo();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cut cut = new Cut();
            cut.Execute(codeeditor.CodeBox);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Copy copy = new Copy();

            if (GlobalVars.IsCodeBoxActive == true)
                copy.Execute(codeeditor.CodeBox);
            //else
                //Clipboard.SetText(codeeditor.FindText.Text);
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Paste paste = new Paste();

            if (GlobalVars.IsCodeBoxActive == true)
                paste.Execute(codeeditor.CodeBox);
            /*else
                codeeditor.FindText.Text = Clipboard.GetText();*/
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (GlobalVars.FindReplaceShowed == false)
            {
                findreplace.Show(this);
                findreplace.FindWhat.Select();

                GlobalVars.FindReplaceShowed = true;
            }
            else
            {
                findreplace.TabControl.SelectedIndex = 0;
            }
        }

        private void findNextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            codeeditor.FindNextMatch(GlobalVars.FindText, true, GlobalVars.CaseSensitiveChecked, GlobalVars.WholeWordChecked);
        }

        private void findPrevToolStripMenuItem_Click(object sender, EventArgs e)
        {
            codeeditor.FindPrevious(GlobalVars.FindText, true, GlobalVars.CaseSensitiveChecked, GlobalVars.WholeWordChecked);
        }

        private void replaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (GlobalVars.FindReplaceShowed == false)
            {
                findreplace.TabControl.SelectedIndex = 1;
                findreplace.Height = findreplace.Height - 70;

                findreplace.Show(this);
                GlobalVars.FindReplaceShowed = true;
            }
            else
            {
                findreplace.TabControl.SelectedIndex = 1;
            }
        }

        private void goToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GoToLine gotoline = new GoToLine();

            gotoline.CurrentLineText.Text = (codeeditor.CodeBox.ActiveTextAreaControl.TextArea.Caret.Line + 1).ToString();
            gotoline.MaximumLineText.Text = codeeditor.CodeBox.Document.TotalNumberOfLines.ToString();
            gotoline.GoToLineText.Text = (codeeditor.CodeBox.ActiveTextAreaControl.TextArea.Caret.Line + 1).ToString();
            gotoline.GoToLineText.Select();

            gotoline.ShowDialog(codeeditor);
        }
        private void compileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StatutsBar.BackColor = GlobalVars.IdleColor;
            StatutsLabel.Text = Languages.Compiling;

            if (codeeditor.CodeBox.Document.TextLength > 0)
            {
                if (GlobalVars.OpenedFilePath != null)
                {
                    if (GlobalVars.CompilerWorking == false)
                    {
                        GlobalVars.CompilerWorking = true;
                        File.WriteAllText(GlobalVars.OpenedFilePath, codeeditor.CodeBox.Text, Encoding.Default);
                        GlobalVars.FileSaved = true;
                        GlobalVars.CompilingText = codeeditor.CodeBox.Document.TextContent;
                        CompilerWorker.RunWorkerAsync();

                    }
                }
                else
                {
                    saveFileDialog.ShowDialog();
                }
            }
            else
            {
                StatutsBar.BackColor = GlobalVars.ErrorColor;
                StatutsLabel.Text = Languages.CantCompile;
            }
        }

        private void CompilerWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var CompilerTime = Stopwatch.StartNew();

            Process Compiling = new Process();

            string TempFile = Environment.CurrentDirectory + "\\" + Path.GetFileNameWithoutExtension(GlobalVars.OpenedFilePath) + ".p";
            string AmxPath = Path.GetDirectoryName(GlobalVars.OpenedFilePath) + "\\" + Path.GetFileNameWithoutExtension(GlobalVars.OpenedFilePath);
            MessageBox.Show(AmxPath);
            StreamWriter PFile = new StreamWriter(TempFile, false, Encoding.Default);
            PFile.Write(GlobalVars.CompilingText);
            PFile.Close();

            StreamWriter PwnFile = new StreamWriter(GlobalVars.OpenedFilePath, false, Encoding.Default);
            PwnFile.Write(GlobalVars.CompilingText);
            PwnFile.Close();

            string PawnoFolder = Directory.GetParent(Directory.GetParent(GlobalVars.OpenedFilePath).ToString()).ToString() + "\\pawno";

            Compiling.StartInfo.FileName = PawnoFolder + "\\" + "pawncc.exe";
            Compiling.StartInfo.Arguments = Path.GetFileNameWithoutExtension(GlobalVars.OpenedFilePath) + " -o\"" + AmxPath + "\" -;+ -(+ " + GlobalVars.CompilerArgs;
            Compiling.StartInfo.UseShellExecute = false;
            Compiling.StartInfo.CreateNoWindow = true;
            Compiling.StartInfo.RedirectStandardError = true;
            Compiling.StartInfo.RedirectStandardOutput = true;
            Compiling.Start();

            GlobalVars.FileSaved = true;

            while (!Compiling.HasExited)
            {
                GlobalVars.Errors = Compiling.StandardError.ReadToEnd();
                GlobalVars.MsgOutput = Compiling.StandardOutput.ReadToEnd();
            }

            File.Delete(TempFile);

            // Thanks maddinat0r about the Regex!
            if (GlobalVars.Errors.Contains("error") == true || GlobalVars.Errors.Contains("warning") == true)
            {
                foreach (Match ErrorMessage in Regex.Matches(GlobalVars.Errors, @"\((.+)\)\s\:\s(.+)\r\n"))
                {
                    if (ErrorMessage.Groups[2].ToString().Contains("undefined") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + " at line " + ErrorMessage.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (ErrorMessage.Groups[2].ToString().Contains("already defined") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[1].ToString() + " at line " + ErrorMessage.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (ErrorMessage.Groups[2].ToString().Contains("array index out") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + " at line " + ErrorMessage.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (ErrorMessage.Groups[2].ToString().Contains("invalid expression") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + " at line " + ErrorMessage.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (ErrorMessage.Groups[2].ToString().Contains("expression has no effect") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + " at line " + ErrorMessage.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (ErrorMessage.Groups[2].ToString().Contains("expression has") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + " at line " + ErrorMessage.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (ErrorMessage.Groups[2].ToString().Contains("number of arguments") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + " at line " + ErrorMessage.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (ErrorMessage.Groups[2].ToString().Contains("loose indentation") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + " at line " + ErrorMessage.Groups[1].ToString() + "." + Environment.NewLine;
                    else if (ErrorMessage.Groups[2].ToString().Contains("symbol is never used") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + "." + Environment.NewLine;
                    else if (ErrorMessage.Groups[2].ToString().Contains("fatal error 100:") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + "." + Environment.NewLine;
                    else if (ErrorMessage.Groups[2].ToString().Contains("too many error") == true)
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + Environment.NewLine;
                    else
                        GlobalVars.ErrorMessage += "> " + ErrorMessage.Groups[2].ToString() + " before line " + ErrorMessage.Groups[1].ToString() + "." + Environment.NewLine;
                }
            }
            CompilerTime.Stop();
            GlobalVars.CompiledSeconds = CompilerTime.Elapsed.TotalSeconds;
            GlobalVars.CompiledTime = CompilerTime.Elapsed.ToString("mm\\:ss");
        }

        private void CompilerWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            output.OutBox.Clear();

            if (GlobalVars.Errors.Length > 0)
                output.OutBox.AppendText(GlobalVars.ErrorMessage + Environment.NewLine);
            output.OutBox.AppendText(GlobalVars.MsgOutput);

            if (output.OutBox.Text.Contains("error") == false)
            {
                StatutsBar.BackColor = GlobalVars.InfoColor;
                if (GlobalVars.CompiledSeconds > 60)
                    StatutsLabel.Text = Languages.CompilingSuccesful + GlobalVars.CompiledTime + Languages.CompilingSuccesfulMinutes;
                else
                    StatutsLabel.Text = Languages.CompilingSuccesful + GlobalVars.CompiledTime + Languages.CompilingSuccesfulSeconds;
            }
            else
            {
                StatutsBar.BackColor = GlobalVars.ErrorColor;
                StatutsLabel.Text = Languages.CompilingError;
            }

            GlobalVars.CompilerWorking = false;
            GlobalVars.ErrorMessage = null;

            SystemSounds.Asterisk.Play();
        }

        #region Principal Functions
        private void MinimizeButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void MaximizeButton_Click(object sender, EventArgs e)
        {
            if (GlobalVars.Maximized == true)
            {
                this.WindowState = FormWindowState.Normal;

                this.Width = Screen.PrimaryScreen.Bounds.Width / 2;
                this.Height = Screen.PrimaryScreen.Bounds.Height / 2;

                int PositionX = Screen.PrimaryScreen.Bounds.Width - this.Width;
                int PositionY = Screen.PrimaryScreen.Bounds.Height - this.Height;

                this.Location = new Point(PositionX / 2, PositionY / 2);


                GlobalVars.Maximized = false;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.MaximumSize = Screen.PrimaryScreen.WorkingArea.Size;

                GlobalVars.Maximized = true;
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PrincipalPanel_DoubleClick(object sender, EventArgs e)
        {
            if (GlobalVars.Maximized == true)
            {
                this.WindowState = FormWindowState.Normal;

                this.Width = Screen.PrimaryScreen.Bounds.Width / 2;
                this.Height = Screen.PrimaryScreen.Bounds.Height / 2;

                int PositionX = Screen.PrimaryScreen.Bounds.Width - this.Width;
                int PositionY = Screen.PrimaryScreen.Bounds.Height - this.Height;

                this.Location = new Point(PositionX / 2, PositionY / 2);

                GlobalVars.Maximized = false;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.MaximumSize = Screen.PrimaryScreen.WorkingArea.Size;

                GlobalVars.Maximized = true;
            }
        }
        #endregion
    }
}
