﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

#region DigitalRune
using DigitalRune.Windows.TextEditor;
using DigitalRune.Windows.TextEditor.Actions;
using DigitalRune.Windows.TextEditor.Completion;
using DigitalRune.Windows.TextEditor.Document;
using DigitalRune.Windows.TextEditor.Formatting;
using DigitalRune.Windows.TextEditor.Highlighting;
using DigitalRune.Windows.TextEditor.Insight;
using DigitalRune.Windows.TextEditor.Markers;
using DigitalRune.Windows.TextEditor.Selection;
using DigitalRune.Windows.TextEditor.Folding;
#endregion

namespace PawnPlus
{
    public partial class GoToLine : Form
    {
        public GoToLine()
        {
            InitializeComponent();

        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(GoToLineText.Text) > GlobalVars.MaximLines || Convert.ToInt32(GoToLineText.Text) < 1 || GoToLineText.Text.Length < 0)
                LineError.SetError(GoToLineText, "Inserted line is invalid");
            else if (GoToLineText.Text.Length > 0)
            {
                GlobalVars.GTLActive = true;
                GlobalVars.GTLine = Convert.ToInt32(GoToLineText.Text);
                this.Close();
            }
        }

        private void GoToLineText_TextChanged(object sender, EventArgs e)
        {
            long LongOut;
            if (!long.TryParse(GoToLineText.Text, out LongOut))
            {
                GoToLineText.Clear();
            }
        }
    }
}
