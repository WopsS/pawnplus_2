﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawnPlus
{
    class GlobalVars
    {
        #region Strings
        public static string OpenedFilePath = null;
        public static string ApplicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PawnPlus";

        public static string CompilingText = null;
        public static string CompilerArgs = null;
        public static string CompiledTime = null;

        public static string Errors = null;
        public static string MsgOutput = null;
        public static string ErrorMessage = null;

        public static string FindText = null;
        public static string ReplaceText = null;
        public static string ReplaceWith = null;
        #endregion

        #region Int
        public static double CompiledSeconds = 0;

        public static int GTLine = 1;
        public static int MaximLines = 0;
        #endregion

        #region Bools
        public static bool Maximized = true;

        public static bool FileSaved = true;

        public static bool IsCodeBoxActive = true;

        public static bool CompilerWorking = false;

        public static bool GTLActive = false;

        public static bool FindClicked = false;
        public static bool CaseSensitiveChecked = false;
        public static bool WholeWordChecked = false;
        public static bool ReplaceClicked = false;
        public static bool ReplacePreviousClicked = false;
        public static bool ReplaceAllClicked = false;
        public static bool FindReplaceShowed = false;
        #endregion

        #region Colors

        public static Color ReadyColor = Color.FromArgb(0, 122, 204);
        public static Color IdleColor = Color.FromArgb(202, 81, 0);
        public static Color InfoColor = Color.FromArgb(10, 66, 103);
        public static Color ErrorColor = Color.FromArgb(170, 0, 0);

        #endregion

        public static Version CurrentVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;

    }
}
