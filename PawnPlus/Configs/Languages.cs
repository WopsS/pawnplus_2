﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawnPlus
{
    class Languages
    {
        #region LanguageStrings
        public static string Version = null;
        public static string Beta = null;

        public static string Ready = null;

        public static string SaveChangesTitle = null;
        public static string SaveChanges = null;

        public static string OpeningFile = null;
        public static string FileOpened = null;
        public static string OpeningCanceled = null;

        public static string SavingFile = null;
        public static string FileSaved = null;
        public static string SavingCanceled = null;
        public static string SaveEmptyFile = null;
        public static string CantSave = null;

        public static string FindPrevious = null;
        public static string FindNext = null;

        public static string Compiling = null;
        public static string CantCompile = null;
        public static string CompilingError = null;
        public static string CompilingSuccesful = null;
        public static string CompilingSuccesfulSeconds = null;
        public static string CompilingSuccesfulMinutes = null;
        #endregion

        public static void Language(string language)
        {
            if (language == "English")
            {
                Version = "Version";
                Beta = "beta";

                Ready = "Ready";

                SaveChangesTitle = "Save changes";
                SaveChanges = "You want to save your changes?";

                OpeningFile = "Opening file...";
                FileOpened = "File opened";
                OpeningCanceled = "Opening file canceled";

                SavingFile = "Saving file...";
                FileSaved = "File saved";
                SavingCanceled = "Saving file canceled";
                SaveEmptyFile = "Can't save empty file";
                CantSave = "You can't have while compiler working";

                FindPrevious = "Find Previous (temporary not working)";
                FindNext = "Find Next (F3)";

                Compiling = "Compiling...";
                CantCompile = "Can't compile an empty file.";
                CompilingError = "Can't compile, here are some errors";
                CompilingSuccesful = "File compiled in ";
                CompilingSuccesfulMinutes = " minutes";
                CompilingSuccesfulSeconds = " seconds";
            }
        }
    }
}
