;PawnPlus Installer

;--------------------------------
;Includes

	!include "MUI2.nsh"
	!include "WordFunc.nsh"
	
	!define MUI_ICON "C:\Users\WopsS\Documents\Visual Studio 2013\Projects\PawnPlus\PawnPlus\Resources\Installer.ico"
	
	!define MUI_WELCOMEFINISHPAGE_BITMAP "C:\Users\WopsS\Documents\Visual Studio 2013\Projects\PawnPlus\PawnPlus\Resources\PawnPlus_Install_Dialog.bmp" 
	!define MUI_HEADERIMAGE
	!define MUI_HEADERIMAGE_RIGHT
	!define MUI_HEADERIMAGE_BITMAP "C:\Users\WopsS\Documents\Visual Studio 2013\Projects\PawnPlus\PawnPlus\Resources\PawnPlus_Install_Banner.bmp"  
	
	!define CurrentVersion 0.2.0
	!insertmacro VersionCompare
;--------------------------------
;General

	Name "PawnPlus"
	OutFile "PawnPlus-setup.exe"
	
	InstallDir "$PROGRAMFILES\PawnPlus"
	
	InstallDirRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus" "Install Location"

	RequestExecutionLevel user
;--------------------------------
;Interface Settings

	!define MUI_ABORTWARNING

	!define MUI_LANGDLL_ALLLANGUAGES

;--------------------------------
;Language Selection Dialog Settings

	!define MUI_LANGDLL_REGISTRY_ROOT "HKCU" 
	!define MUI_LANGDLL_REGISTRY_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus" 
	!define MUI_LANGDLL_REGISTRY_VALUENAME "Installer Language"

;--------------------------------
;Pages

	!insertmacro MUI_PAGE_WELCOME
	!insertmacro MUI_PAGE_LICENSE "C:\Users\WopsS\Documents\Visual Studio 2013\Projects\PawnPlus\License"
	!insertmacro MUI_PAGE_DIRECTORY
	!insertmacro MUI_PAGE_INSTFILES
	!insertmacro MUI_PAGE_FINISH
	
	!insertmacro MUI_UNPAGE_CONFIRM
	!insertmacro MUI_UNPAGE_INSTFILES
;--------------------------------
;Languages

	!insertmacro MUI_LANGUAGE "English"
	!insertmacro MUI_LANGUAGE "Romanian"
	!insertmacro MUI_LANGUAGE "Russian"

;--------------------------------
;Version Information

	VIProductVersion "0.2.0.0"
	VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "PawnPlus"
	VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" ""
	VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" ""
	VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "Dima Octavian"
	VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" ""
	VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${CurrentVersion}"
	VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductVersion" "${CurrentVersion}"
	
;--------------------------------
;Reserve Files

	!insertmacro MUI_RESERVEFILE_LANGDLL

;--------------------------------
;Installer Sections

Var UNINSTALL_OLD_VERSION

Section "PawnPlus Section" PawnPlusSection
	StrCmp $UNINSTALL_OLD_VERSION "" core.files
	ExecWait '$UNINSTALL_OLD_VERSION'

	core.files:
		SetOutPath "$INSTDIR"
	
		File /r "C:\Users\WopsS\Documents\Visual Studio 2013\Projects\PawnPlus\PawnPlus\bin\Release\*.*"
	
		CreateShortCut "$DESKTOP\PawnPlus.lnk" "$INSTDIR\PawnPlus.exe" ""
 
		CreateDirectory "$SMPROGRAMS\PawnPlus"
		CreateShortCut "$SMPROGRAMS\PawnPlus\Uninstall.lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\Uninstall.exe" 0
		CreateShortCut "$SMPROGRAMS\PawnPlus\PawnPlus.lnk" "$INSTDIR\PawnPlus.exe" "" "$INSTDIR\PawnPlus.exe" 0

		WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus" "DisplayName" "PawnPlus"
		WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus" "DisplayVersion" "${CurrentVersion}"
		WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus" "DisplayIcon" "$INSTDIR\PawnPlus.exe"
		WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus" "Publisher" "Dima Octavian"
		WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus" "UninstallString" "$INSTDIR\Uninstall.exe"
	
		WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus" "Install Location" $INSTDIR
	
		WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

;--------------------------------
;Installer Functions

Function .onInit

	!insertmacro MUI_LANGDLL_DISPLAY
	
	ClearErrors
	ReadRegStr $0 HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus" "DisplayVersion"
	IfErrors init.uninst
	${VersionCompare} $0 ${CurrentVersion} $1
	IntCmp $1 2 init.uninst
    MessageBox MB_YESNO|MB_ICONQUESTION "PawnPlus seems to be already installed on your system, current version is $0.$\nWould you like to proceed with the installation of version ${CurrentVersion}?" \
        IDYES init.uninst
    Quit

	init.uninst:
		ClearErrors
		ReadRegStr $0 HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus" "Install Location"
		IfErrors init.done
		StrCpy $UNINSTALL_OLD_VERSION '"$0\uninstall.exe" /S _?=$0'
	init.done:

FunctionEnd
;--------------------------------
;Uninstaller Section

Section "Uninstall"

	Delete "$INSTDIR\*.*"
	Delete "$INSTDIR\SAMP\*.*"
	Delete "$INSTDIR\Syntax\*.*"

	Delete "$INSTDIR\Uninstall.exe"
	
	RMDir "$INSTDIR\SAMP\"
	RMDir "$INSTDIR\Syntax\"
	RMDir "$INSTDIR\"

	Delete "$DESKTOP\PawnPlus.lnk"
	Delete "$SMPROGRAMS\PawnPlus\*.*"
	
	DeleteRegKey HKCU "Software\PawnPlus"
	DeleteRegKey HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\PawnPlus"

SectionEnd


;--------------------------------
;Uninstaller Functions

Function un.onInit

	!insertmacro MUI_UNGETLANGUAGE
	
FunctionEnd