﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PawnPlus
{
    public partial class FindReplace : Form
    {
        Point InitialPosition;
        Point MovedPosition;
        bool FormClicked = false;

        public FindReplace()
        {
            InitializeComponent();
            this.TransparencyKey = Color.AliceBlue;
            this.BackColor = Color.AliceBlue;
        }

        #region Move this form
        private void Find_MouseDown(object sender, MouseEventArgs e)
        {
            FormClicked = true;
            InitialPosition = Cursor.Position;
            MovedPosition = this.Location;
        }

        private void Find_MouseMove(object sender, MouseEventArgs e)
        {
            if (FormClicked == true)
            {
                Point NewPoint = Point.Subtract(Cursor.Position, new Size(InitialPosition));
                this.Location = Point.Add(MovedPosition, new Size(NewPoint));
            }
        }

        private void Find_MouseUp(object sender, MouseEventArgs e)
        {
            FormClicked = false;
        }

        private void TabControl_MouseDown(object sender, MouseEventArgs e)
        {
            FormClicked = true;
            InitialPosition = Cursor.Position;
            MovedPosition = this.Location;
        }

        private void TabControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (FormClicked == true)
            {
                Point NewPoint = Point.Subtract(Cursor.Position, new Size(InitialPosition));
                this.Location = Point.Add(MovedPosition, new Size(NewPoint));
            }
        }

        private void TabControl_MouseUp(object sender, MouseEventArgs e)
        {
            FormClicked = false;
        }

        private void FindTab_MouseDown(object sender, MouseEventArgs e)
        {
            FormClicked = true;
            InitialPosition = Cursor.Position;
            MovedPosition = this.Location;
        }

        private void FindTab_MouseMove(object sender, MouseEventArgs e)
        {
            if (FormClicked == true)
            {
                Point NewPoint = Point.Subtract(Cursor.Position, new Size(InitialPosition));
                this.Location = Point.Add(MovedPosition, new Size(NewPoint));
            }
        }

        private void FindTab_MouseUp(object sender, MouseEventArgs e)
        {
            FormClicked = false;
        }

        private void ReplaceTab_MouseDown(object sender, MouseEventArgs e)
        {
            FormClicked = true;
            InitialPosition = Cursor.Position;
            MovedPosition = this.Location;
        }

        private void ReplaceTab_MouseMove(object sender, MouseEventArgs e)
        {
            if (FormClicked == true)
            {
                Point NewPoint = Point.Subtract(Cursor.Position, new Size(InitialPosition));
                this.Location = Point.Add(MovedPosition, new Size(NewPoint));
            }
        }

        private void ReplaceTab_MouseUp(object sender, MouseEventArgs e)
        {
            FormClicked = false;
        }
        #endregion

        private void close_Click(object sender, EventArgs e)
        {
            GlobalVars.FindReplaceShowed = false;
            this.Hide();
        }

        private void close2_Click(object sender, EventArgs e)
        {
            GlobalVars.FindReplaceShowed = false;
            this.Hide();
        }

        private void FindTab_Enter(object sender, EventArgs e)
        {
            this.Height = this.Height - 70;
        }

        private void ReplaceTab_Enter(object sender, EventArgs e)
        {
            this.Height = this.Height + 70;
        }

        private void findnext_Click(object sender, EventArgs e)
        {
            GlobalVars.FindClicked = true;

            if (!string.IsNullOrEmpty(FindWhat.Text))
                GlobalVars.FindText = FindWhat.Text;
        }

        private void casesensitive_CheckStateChanged(object sender, EventArgs e)
        {
            GlobalVars.CaseSensitiveChecked = casesensitive.Checked;
            matchwholeword2.Checked = false;
        }

        private void matchwholeword_CheckStateChanged(object sender, EventArgs e)
        {
            GlobalVars.WholeWordChecked = matchwholeword.Checked;
            casesensitive.Checked = false;
        }

        private void ReplacePrevious_Click(object sender, EventArgs e)
        {
            GlobalVars.ReplacePreviousClicked = true;

            if (!string.IsNullOrEmpty(FindWhat2.Text))
                GlobalVars.ReplaceText = FindWhat2.Text;

            GlobalVars.ReplaceWith = ReplaceWithText.Text;
        }

        private void ReplaceNext_Click(object sender, EventArgs e)
        {
            GlobalVars.ReplaceClicked = true;

            if (!string.IsNullOrEmpty(FindWhat2.Text))
                GlobalVars.ReplaceText = FindWhat2.Text;

            GlobalVars.ReplaceWith = ReplaceWithText.Text;
        }

        private void ReplaceAll_Click(object sender, EventArgs e)
        {
            GlobalVars.ReplaceAllClicked = true;

            if (!string.IsNullOrEmpty(FindWhat2.Text))
                GlobalVars.ReplaceText = FindWhat2.Text;

            GlobalVars.ReplaceWith = ReplaceWithText.Text;
        }

        private void casesensitive2_CheckStateChanged(object sender, EventArgs e)
        {
            matchwholeword2.Checked = false;
        }

        private void matchwholeword2_CheckStateChanged(object sender, EventArgs e)
        {
            casesensitive2.Checked = false;
        }

        private void FindTab_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if(e.KeyCode == Keys.Return)
            {
                GlobalVars.FindClicked = true;
            }
        }

        private void FindWhat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                findnext.Select();
                SendKeys.Send("{ENTER}");
            }
        }
    }
}
