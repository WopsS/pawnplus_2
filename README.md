What is PawnPlus?
========

<B>PawnPlus</B> is a <b>Pawn++ IDE</b>, it isn't a compiler, with more features than <B>PAWNO</B>.

How I install it?
========

Simply, just download <b>.exe</b> file from here https://github.com/WopsS/PawnPlus/releases and run it.

Requirements
========

You just need only .NET FrameWork 4.0 or highter and Windows XP / 7 / 8.

<B>PawnPlus</B> was tested only on Windows XP /  7 / 8 / 8.1.

License Agreement
========

<B>PawnPlus</B> is open-source IDE for San Andreas Multiplayer, nobody has the right to sell it / redistributes / etc. without my permission.

Copyright
========

<B>PawnPlus IDE</B> or <B>PawnPlus</B>  is copyrighted by <i>Dima Octavian</i> (me). <b>Nobody don't have copyright on it.</b>

<a target="_blank" href="http://www.copyrighted.com/copyrights/view/8gsu-segr-lbsw-hb9o"><img border="0" alt="Copyrighted.com Registered &amp; Protected 
8GSU-SEGR-LBSW-HB9O" title="Copyrighted.com Registered &amp; Protected 
8GSU-SEGR-LBSW-HB9O" width="150" height="40" src="http://static.copyrighted.com/images/seal.gif" /></a>
